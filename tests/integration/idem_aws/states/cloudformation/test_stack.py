import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_stack(hub, ctx, aws_ec2_instance):
    key_name_for_test = aws_ec2_instance["changes"]["new"]["KeyName"]

    name = "Cloudformation-Stack-Testing-idem"
    resource_id = "CF" + str(uuid.uuid4())
    template_url = "https://s3-us-west-1.amazonaws.com/cf-templates-yaybgi058f7-us-west-1/2022094Ukx-EC2InstanceWithSecurityGroupSample.template"
    parameters = [
        {"ParameterKey": "KeyName", "ParameterValue": key_name_for_test},
        {"ParameterKey": "SSHLocation", "ParameterValue": "0.0.0.0/0"},
        {"ParameterKey": "InstanceType", "ParameterValue": "t2.micro"},
    ]

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Idem test -- Create
    ret = await hub.states.aws.cloudformation.stack.present(
        test_ctx,
        name=name,
        resource_id=resource_id,
        parameters=parameters,
        template_url=template_url,
    )
    assert ret["result"], ret["comment"]
    assert f"Would create aws.cloudformation.stack '{resource_id}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert resource_id == resource.get("resource_id")
    assert parameters == resource.get("parameters")

    # Real Test - Create Stack
    ret = await hub.states.aws.cloudformation.stack.present(
        ctx,
        name=name,
        resource_id=resource_id,
        parameters=parameters,
        template_url=template_url,
    )
    assert ret["result"], ret["comment"]
    assert f"Created aws.cloudformation.stack '{resource_id}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    stack_id = resource.get("stack_id")
    assert resource_id == resource.get("resource_id")
    assert parameters == resource.get("parameters")

    # Describe
    describe_ret = await hub.states.aws.cloudformation.stack.describe(ctx)
    described_resource = describe_ret.get(stack_id).get(
        "aws.cloudformation.stack.present"
    )
    described_resource_chain_map = dict(ChainMap(*described_resource))

    assert "resource_id" in described_resource_chain_map
    assert "name" in described_resource_chain_map
    assert "parameters" in described_resource_chain_map
    assert "resource_id" in described_resource_chain_map

    # Idem test - Update stack
    updated_parameters = [
        {"ParameterKey": "KeyName", "ParameterValue": key_name_for_test},
        {"ParameterKey": "SSHLocation", "ParameterValue": "0.0.0.0/0"},
        {"ParameterKey": "InstanceType", "ParameterValue": "t2.small"},
    ]
    update_ret = await hub.states.aws.cloudformation.stack.present(
        test_ctx,
        name=name,
        resource_id=resource_id,
        template_url=template_url,
        parameters=updated_parameters,
    )
    assert update_ret["result"], update_ret["comment"]
    assert f"Would update aws.cloudformation.stack '{name}'" in update_ret["comment"]
    assert update_ret.get("old_state") and update_ret.get("new_state")
    resource = update_ret.get("new_state")
    assert updated_parameters == resource.get("parameters")

    # Real Test - Update Stack
    updated_parameters = [
        {"ParameterKey": "KeyName", "ParameterValue": key_name_for_test},
        {"ParameterKey": "SSHLocation", "ParameterValue": "0.0.0.0/0"},
        {"ParameterKey": "InstanceType", "ParameterValue": "t2.small"},
    ]
    update_ret = await hub.states.aws.cloudformation.stack.present(
        ctx,
        name=name,
        resource_id=resource_id,
        template_url=template_url,
        parameters=updated_parameters,
    )
    assert update_ret["result"], update_ret["comment"]
    assert f"Updated aws.cloudformation.stack '{resource_id}'" in update_ret["comment"]
    assert update_ret.get("old_state") and update_ret.get("new_state")
    resource = update_ret.get("new_state")
    assert updated_parameters == resource.get("parameters")

    # Idem test -- delete
    delete_ret = await hub.states.aws.cloudformation.stack.absent(
        test_ctx, name=name, resource_id=resource_id
    )
    assert delete_ret["result"], delete_ret["comment"]
    assert delete_ret["old_state"]
    assert not delete_ret["new_state"]
    assert (
        f"Would delete aws.cloudformation.stack '{resource_id}'"
        in delete_ret["comment"]
    )

    # Real Test - Delete Stack
    delete_ret = await hub.states.aws.cloudformation.stack.absent(
        ctx, name=name, resource_id=resource_id
    )
    assert delete_ret["result"] is True
    assert delete_ret["old_state"]
    assert not delete_ret["new_state"]
    assert f"Deleted '{resource_id}'" in delete_ret["comment"]

    # Real Test - Delete Stack Again
    delete_ret = await hub.states.aws.cloudformation.stack.absent(
        ctx, name=name, resource_id=resource_id
    )
    assert delete_ret["result"], delete_ret["comment"]
    assert (not delete_ret["old_state"]) and (not delete_ret["new_state"])
    assert (
        f"aws.cloudformation.stack '{resource_id}' already absent"
        in delete_ret["comment"]
    )
