from collections import OrderedDict
from typing import Any
from typing import Dict

"""
Util functions to convert raw resource state from AWS Cloudformation Stack to present input format.
"""


def convert_raw_stack_to_present(
    hub, raw_resource: Dict[str, Any], idem_resource_name: str = None
) -> Dict[str, Any]:
    hub.log.debug(
        f"Converting raw aws.cloudformation.stack: {raw_resource} to valid state"
    )
    describe_params = OrderedDict(
        {
            "StackName": "resource_id",
            "StackId": "stack_id",
            "TemplateBody": "template_body",
            "TemplateUrl": "template_url",
            "Parameters": "parameters",
            "DisableRollback": "disable_rollback",
            "RollbackConfiguration": "rollback_configuration",
            "TimeoutInMinutes": "timeout_in_minutes",
            "NotificationARNs": "notification_ar_ns",
            "Capabilities": "capabilities",
            "ResourceTypes": "resource_types",
            "RoleARN": "role_arn",
            "OnFailure": "on_failure",
            "StackPolicyBody": "stack_policy_body",
            "StackPolicyURL": "stack_policy_url",
            "Tags": "tags",
            "ClientRequestToken": "client_request_token",
            "EnableTerminationProtection": "enable_termination_protection",
        }
    )

    resource_id = raw_resource.get("StackId")

    resource_translated = {"name": idem_resource_name, "resource_id": resource_id}
    for parameter_raw, parameter_present in describe_params.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_translated[parameter_present] = raw_resource.get(parameter_raw)

    return resource_translated
