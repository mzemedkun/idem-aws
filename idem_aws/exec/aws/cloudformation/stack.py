from typing import Dict
from typing import List

from dict_tools import differ


async def update_stack_options(
    hub,
    ctx,
    stack_name: str,
    template_body=None,
    template_url=None,
    parameters=[],
    rollback_configuration=None,
    resource_types=None,
    notification_ar_ns=[],
    capabilities=None,
    role_arn=None,
    stack_policy_body: str = None,
    stack_policy_url: str = None,
    tags: List = None,
    client_request_token: str = None,
    timeout: Dict = None,
) -> Dict[str, any]:
    """
    Args:
        notification_ar_ns:
        hub:
        ctx:
        template_body(string):
        template_url(string):
        parameters(List):
        capabilities(List):
        resource-types(List):
        role-arn(string):
        rollback-configuration(bool):
        stack-policy-body(string):
        stack-policy-url(string):
        notification-arns(List):
        tag(List):
        disable-rollback(bool):
        client-request-token(string):
        cli-input-json(string):

        Returns:
            {"result": True|False, "comment": "A message tuple", "ret": None}
    """

    result = dict(comment=(), result=True, ret=None)
    before = await hub.exec.boto3.client.cloudformation.describe_stacks(
        ctx, StackName=stack_name
    )

    if before["result"]:
        hub.log.debug(f"Updating Stack: {stack_name}")
        old_state = (
            hub.tool.aws.cloudformation.conversion_utils.convert_raw_stack_to_present(
                raw_resource=before["ret"]["Stacks"][0]
            )
        )

        parameters if parameters != old_state.get("parameters") else old_state.get(
            "parameters"
        )
        template_url if template_url != parameters != old_state.get(
            "template_url"
        ) else old_state.get("template_url")
        template_body if template_body != old_state.get(
            "template_body"
        ) else old_state.get("template_body")
        tags if tags != old_state.get("tags") else old_state.get("tags")
        capabilities if capabilities != old_state.get(
            "capabilities"
        ) else old_state.get("capabilities")
        rollback_configuration if rollback_configuration != old_state.get(
            "rollback_configuration"
        ) else old_state.get("rollback_configuration")
        role_arn if role_arn != old_state.get("role_arn") else old_state.get("role_arn")
        notification_ar_ns if notification_ar_ns != old_state.get(
            "notification_ar_ns"
        ) else old_state.get("notification_ar_ns")
        stack_policy_body if stack_policy_body != old_state.get(
            "stack_policy_body"
        ) else old_state.get("stack_policy_body")
        stack_policy_url if stack_policy_url != old_state.get(
            "stack_policy_url"
        ) else old_state.get("stack_policy_url")
        resource_types if resource_types != old_state.get(
            "resource_types"
        ) else old_state.get("resource_types")
        client_request_token if client_request_token != old_state.get(
            "client_request_token"
        ) else old_state.get("client_request_token")

        if ctx.get("test", False) and result["result"]:
            plan_state = hub.tool.aws.test_state_utils.generate_test_state(
                enforced_state=old_state,
                desired_state={
                    "stack_name": stack_name,
                    "template_url": template_url,
                    "parameters": parameters,
                },
            )
            result["new_state"] = {
                key: val for key, val in plan_state.items() if val is not None
            }
            result["comment"] = (
                f"Would succeed in modifying aws.cloudformation.stack '{stack_name}'",
            )
            return result

        ret = await hub.exec.boto3.client.cloudformation.update_stack(
            ctx,
            StackName=stack_name,
            TemplateBody=template_body,
            TemplateURL=template_url,
            Parameters=parameters,
            StackPolicyBody=stack_policy_body,
            StackPolicyURL=stack_policy_url,
            Capabilities=capabilities,
            ResourceTypes=resource_types,
            RoleARN=role_arn,
            RollbackConfiguration=rollback_configuration,
            Tags=tags,
            ClientRequestToken=client_request_token,
        )
        if not ret["result"]:
            result["comment"] = ret["comment"]
            result["result"] = False
            result["comment"]
            return result
        update_waiter_acceptors = [
            {
                "matcher": "pathAll",
                "expected": "UPDATE_COMPLETE",
                "state": "success",
                "argument": "Stacks[].StackStatus",
            },
            {
                "matcher": "pathAll",
                "expected": "UPDATE_IN_PROGRESS",
                "state": "retry",
                "argument": "Stacks[].StackStatus",
            },
            {
                "matcher": "pathAll",
                "expected": "ROLLBACK_COMPLETE",
                "state": "retry",
                "argument": "Stacks[].StackStatus",
            },
            {
                "matcher": "pathAll",
                "expected": "CREATE_IN_PROGRESS",
                "state": "retry",
                "argument": "Stacks[].StackStatus",
            },
            {
                "matcher": "pathAll",
                "expected": "ROLLBACK_COMPLETE",
                "state": "retry",
                "argument": "Stacks[].StackStatus",
            },
        ]
        waiter_config = hub.tool.aws.waiter_utils.create_waiter_config(
            default_delay=10,
            default_max_attempts=60,
            timeout_config=timeout.get("update") if timeout else None,
        )
        endpoint_waiter = hub.tool.boto3.custom_waiter.waiter_wrapper(
            name="StackUpdate",
            operation="DescribeStacks",
            argument=["Stacks[].StackStatus"],
            acceptors=update_waiter_acceptors,
            client=hub.tool.boto3.client.get_client(ctx, "cloudformation"),
        )
        try:
            await hub.tool.boto3.client.wait(
                ctx,
                "cloudformation",
                "StackUpdate",
                endpoint_waiter,
                StackName=stack_name,
                WaiterConfig=waiter_config,
            )
        except Exception as e:
            result["comment"] = str(e)
            result["result"] = False

    else:
        hub.log.debug(
            f"aws.cloudformation.stack {stack_name} does not exist and cannot be updated"
        )
        result["comment"] = f"aws.cloudformation.stack {stack_name} does not exist."
        result["result"] = False
    return result
